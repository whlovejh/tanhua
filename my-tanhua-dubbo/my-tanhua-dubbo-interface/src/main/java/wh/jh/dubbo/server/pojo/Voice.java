package wh.jh.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "voice")
public class Voice implements Serializable {
    private ObjectId id;
    @Indexed// 增加索引,保证查询效率
    private Long userId;
    private String avatar; //头像
    private String nickname; //昵称
    private String gender; //性别
    private Integer age; //昵称
    private String soundUrl; //声音连接
    private Long date; //时间
}

