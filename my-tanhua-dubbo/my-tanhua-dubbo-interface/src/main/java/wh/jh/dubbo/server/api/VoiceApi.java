package wh.jh.dubbo.server.api;

import wh.jh.dubbo.server.pojo.Voice;

public interface VoiceApi {
    /**
     * @author  官文皓
     * @create  2021/6/1
     * @desc    上传语音
     **/
    public Boolean saveVoice(Voice voice);

    /**
     * @author  官文皓
     * @create  2021/6/1
     * @desc    随机获取语音
     **/
    public Voice getVoice(Long userId);
}
