package wh.jh.dubbo.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import wh.jh.dubbo.server.pojo.HuanXinUser;

@Component
public interface HuanXinUserMapper extends BaseMapper<HuanXinUser> {
}
