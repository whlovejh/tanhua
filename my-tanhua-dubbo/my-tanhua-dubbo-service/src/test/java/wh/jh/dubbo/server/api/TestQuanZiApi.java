package wh.jh.dubbo.server.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import wh.jh.dubbo.server.pojo.Publish;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestQuanZiApi {

    @Autowired
    private QuanZiApi quanZiApi;

    @Test
    public void testQueryPublishList(){
        this.quanZiApi.queryPublishList(1L, 1, 2)
                .getRecords().forEach(publish -> System.out.println(publish));
        System.out.println("------------");
        this.quanZiApi.queryPublishList(1L, 2, 2)
                .getRecords().forEach(publish -> System.out.println(publish));
        System.out.println("------------");
        this.quanZiApi.queryPublishList(1L, 3, 2)
                .getRecords().forEach(publish -> System.out.println(publish));

    }


    @Test
    public void testLike(){
        Long userId = 61L;
        String publishId = "5e82dc3e6401952928c211a3";
//        Boolean data = this.quanZiApi.queryUserIsLike(userId, publishId);
//        System.out.println(data);

        System.out.println(this.quanZiApi.likeComment(userId, publishId));

//        System.out.println(this.quanZiApi.queryLikeCount(publishId));
//
//        System.out.println(this.quanZiApi.disLikeComment(userId, publishId));
//
//        System.out.println(this.quanZiApi.queryLikeCount(publishId));
     }
     @Test
    public void getPublishByPublishId(){
         String publishId = "5e82dc3e6401952928c211a3";
         Publish publish = quanZiApi.queryPublishById(publishId);
         System.out.println(publish);
     }
}
