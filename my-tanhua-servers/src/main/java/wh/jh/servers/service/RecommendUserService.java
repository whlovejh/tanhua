package wh.jh.servers.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;
import wh.jh.dubbo.server.api.RecommendUserApi;
import wh.jh.dubbo.server.vo.PageInfo;
import wh.jh.dubbo.server.pojo.RecommendUser;
import wh.jh.servers.vo.TodayBest;

//负责与dubbo交互
@Service
public class RecommendUserService {
    @Reference(version = "1.0.0")
    private RecommendUserApi recommendUserApi;

    public TodayBest queryTodayBest(Long userId) {
        RecommendUser recommendUser = recommendUserApi.queryWithMaxScore(userId);
        TodayBest todayBest = new TodayBest();
        todayBest.setId(recommendUser.getUserId());
        Double score = recommendUser.getScore();
        todayBest.setFateValue(Double.valueOf(score).longValue());
        return todayBest;
    }

    public PageInfo<RecommendUser> queryRecommendUserList(Long userId, Integer page, Integer pageSize) {
        return this.recommendUserApi.queryPageInfo(userId, page, pageSize);
    }


    /**
     * 查询推荐好友的缘分值
     *
     * @param userId
     * @param toUserId
     * @return
     */
    public Double queryScore(Long userId, Long toUserId){
        Double score = this.recommendUserApi.queryScore(userId, toUserId);
        if(ObjectUtil.isNotEmpty(score)){
            return score;
        }
        //默认值
        return 98d;
    }
}
