package wh.jh.servers.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;

import org.springframework.stereotype.Service;
import wh.jh.commons.pojo.User;
import wh.jh.commons.utils.UserThreadLocal;
import wh.jh.dubbo.server.api.HuanXinApi;
import wh.jh.dubbo.server.pojo.HuanXinUser;
import wh.jh.servers.vo.HuanXinUserVo;

@Service
public class HuanXinService {

    @Reference(version = "1.0.0")
    private HuanXinApi huanXinApi;

    public HuanXinUserVo queryHuanXinUser() {
        User user = UserThreadLocal.get();
        //通过dubbo服务查询环信用户
        HuanXinUser huanXinUser = this.huanXinApi.queryHuanXinUser(user.getId());
        if (ObjectUtil.isNotEmpty(huanXinUser)) {
            return new HuanXinUserVo(huanXinUser.getUsername(), huanXinUser.getPassword());
        }
        return null;
    }
}