package wh.jh.servers.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wh.jh.commons.mapper.QuestionMapper;
import wh.jh.commons.pojo.Question;

import java.util.Date;

@Service
public class QuestionService {

    @Autowired
    private QuestionMapper questionMapper;


    public Question queryQuestion(Long userId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userId);
        return this.questionMapper.selectOne(queryWrapper);
    }

    /**
     * 设置陌生人问题
     *
     * @return
     */
    public void save(Long userId, String content) {
        Question question = this.queryQuestion(userId);
        if (null != question){
            question.setTxt(content);
            this.questionMapper.updateById(question);
        }else {
            question = new Question();
            question.setUserId(userId);
            question.setTxt(content);
            question.setCreated(new Date());
            question.setUpdated(question.getCreated());
            this.questionMapper.insert(question);
        }
    }
}
