package wh.jh.servers.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wh.jh.commons.mapper.BlackListMapper;
import wh.jh.commons.pojo.BlackList;

@Service
public class BlackListService {
    @Autowired
    private BlackListMapper blackListMapper;

    public IPage<BlackList> queryBlackList(Long userId,Integer page,Integer pageSize){
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        queryWrapper.orderByDesc("created");
        Page<BlackList> pager =new Page<>(page,pageSize);
        return this.blackListMapper.selectPage(pager,queryWrapper);
    }
    /**
     * 移除黑名单
     *
     * @return
     */
    public Boolean delBlacklist(Long userId, Long blackUserId) {
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId).eq("black_user_id",blackUserId);
        return this.blackListMapper.delete(queryWrapper) > 0;
    }
}
