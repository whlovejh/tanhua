package wh.jh.servers.service;


import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import wh.jh.commons.mapper.SettingsMapper;
import wh.jh.commons.pojo.Settings;

@Service
public class SettingsService {
    @Autowired
    private SettingsMapper settingsMapper;

    /**
     * 根据用户id查询配置
     *
     * @param userId
     * @return
     */
    public Settings querySettings(Long userId){
        QueryWrapper<Settings> queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userId);
        return this.settingsMapper.selectOne(queryWrapper);
    }
    /**
     * 更新通知设置
     *
     */
    public void updateNotification(Long userId, Boolean likeNotification, Boolean pinglunNotification, Boolean gonggaoNotification) {
        QueryWrapper<Settings> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);

        Settings settings = this.settingsMapper.selectOne(queryWrapper);
        if(null == settings){
            //如果没有数据的话，插入一条数据
            settings = new Settings();
            settings.setUserId(userId);
            this.settingsMapper.insert(settings);
        }else{
            //更新
            settings.setLikeNotification(likeNotification);
            settings.setPinglunNotification(pinglunNotification);
            settings.setGonggaoNotification(gonggaoNotification);
            this.settingsMapper.update(settings, queryWrapper);
        }
    }
}
