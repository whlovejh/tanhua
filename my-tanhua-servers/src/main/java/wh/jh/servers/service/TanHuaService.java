package wh.jh.servers.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import wh.jh.commons.enums.SexEnum;
import wh.jh.commons.pojo.Question;
import wh.jh.commons.pojo.User;
import wh.jh.commons.pojo.UserInfo;
import wh.jh.commons.utils.UserThreadLocal;
import wh.jh.dubbo.server.api.*;
import wh.jh.dubbo.server.enums.HuanXinMessageType;
import wh.jh.dubbo.server.pojo.RecommendUser;
import wh.jh.dubbo.server.pojo.UserLocation;
import wh.jh.dubbo.server.vo.PageInfo;
import wh.jh.dubbo.server.vo.UserLocationVo;
import wh.jh.servers.vo.NearUserVo;
import wh.jh.servers.vo.TodayBest;

import javax.swing.plaf.ListUI;
import java.util.*;

@Service
public class TanHuaService {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private RecommendUserService recommendUserService;

    @Autowired
    private QuestionService questionService;

    @Reference(version = "1.0.0")
    private HuanXinApi huanXinApi;


    @Reference(version = "1.0.0")
    private VisitorsApi visitorsApi;

    @Reference(version = "1.0.0")
    private UserLocationApi userLocationApi;

    @Reference(version = "1.0.0")
    private RecommendUserApi recommendUserApi;

    @Reference(version = "1.0.0")
    private UserLikeApi userLikeApi;

    @Autowired
    private IMService imService;

    @Value("${tanhua.default.recommend.users}")
    private String defaultRecommendUsers;

/*    public TodayBest queryUserInfo(Long userId) {

        UserInfo userInfo = this.userInfoService.queryUserInfoByUserId(userId);
        if(ObjectUtil.isEmpty(userInfo)){
            return null;
        }

        TodayBest todayBest = new TodayBest();
        todayBest.setId(userId);
        todayBest.setAge(userInfo.getAge());
        todayBest.setGender(userInfo.getSex().name().toLowerCase());
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setTags(Convert.toStrArray(StrUtil.split(userInfo.getTags(),',')));
        todayBest.setAvatar(userInfo.getLogo());

        //缘分值
        User user = UserThreadLocal.get();
        todayBest.setFateValue(this.recommendUserService.queryScore(userId, user.getId()).longValue());

        return todayBest;
    }*/

    /**
     * 查询陌生人问题
     *
     * @param userId
     * @return
     */
    public String queryQuestion(Long userId) {
        Question question = this.questionService.queryQuestion(userId);
        if(ObjectUtil.isNotEmpty(question)){
            return question.getTxt();
        }
        //默认的问题
        return "你的爱好是什么？";
    }

    /**
     * 回复陌生人问题
     *
     * @return
     */
    public Boolean replyQuestion(Long userId, String reply) {
        User user = UserThreadLocal.get();
        UserInfo userInfo = this.userInfoService.queryUserInfoByUserId(user.getId());

        //构建消息内容
        Map<String, Object> msg = new HashMap<>();
        msg.put("userId", user.getId());
        msg.put("huanXinId", "HX_" + user.getId());
        msg.put("nickname", userInfo.getNickName());
        msg.put("strangerQuestion", this.queryQuestion(userId));
        msg.put("reply", reply);

        //发送环信消息
        return this.huanXinApi.sendMsgFromAdmin("HX_" + userId,
                HuanXinMessageType.TXT, JSONUtil.toJsonStr(msg));
    }

    public TodayBest queryUserInfo(Long userId) {

        UserInfo userInfo = this.userInfoService.queryUserInfoByUserId(userId);
        if(ObjectUtil.isEmpty(userInfo)){
            return null;
        }

        TodayBest todayBest = new TodayBest();
        todayBest.setId(userId);
        todayBest.setAge(userInfo.getAge());
        todayBest.setGender(userInfo.getSex().name().toLowerCase());
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setTags(Convert.toStrArray(StrUtil.split(userInfo.getTags(),',')));
        todayBest.setAvatar(userInfo.getLogo());

        //缘分值
        User user = UserThreadLocal.get();
        todayBest.setFateValue(this.recommendUserService.queryScore(userId, user.getId()).longValue());

        //记录来访用户
        this.visitorsApi.saveVisitor(userId, user.getId(), "个人主页");

        return todayBest;
    }



    /*----------------------------------------------------------------------------------------------------------------------*/
    /**
     * 搜附近
     *
     * @param gender
     * @param distance
     * @return
     */
    public List<NearUserVo> queryNearUser(String gender, String distance) {
        //当前用户信息
        User user = UserThreadLocal.get();
        UserLocationVo userLocationVo = this.userLocationApi.queryByUserId(user.getId());
        if (ObjectUtil.isEmpty(userLocationVo)){
            return ListUtil.empty();
        }
        PageInfo<UserLocationVo> pageInfo = this.userLocationApi.queryUserFromLocation(userLocationVo.getLongitude()
                ,userLocationVo.getLatitude()
        ,Convert.toDouble(distance)
        ,1
        ,50);
        List<UserLocationVo> records = pageInfo.getRecords();
        if (CollUtil.isEmpty(records)){
            return ListUtil.empty();
        }
        List<Object> userIdList = CollUtil.getFieldValues(records,"userId");
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper();
        queryWrapper.in("user_id",userIdList);
        if (StrUtil.equalsIgnoreCase(gender,"man")){
            queryWrapper.eq("sex", SexEnum.MAN);
        }else if (StrUtil.equalsIgnoreCase(gender,"woman")){
            queryWrapper.eq("sex",SexEnum.WOMAN);
        }
        List<UserInfo> userInfoList = this.userInfoService.queryUserInfoList(queryWrapper);
        List<NearUserVo> nearUserVos = new ArrayList<>();
        for (UserLocationVo locationVo : records) {
            //排除自己
            if (ObjectUtil.equals(locationVo.getUserId(),user.getId())){
                continue;
            }

            for (UserInfo userInfo : userInfoList) {
                if(ObjectUtil.equals(locationVo.getUserId(), userInfo.getUserId())){

                    NearUserVo nearUserVo = new NearUserVo();
                    nearUserVo.setUserId(userInfo.getUserId());
                    nearUserVo.setAvatar(userInfo.getLogo());
                    nearUserVo.setNickname(userInfo.getNickName());
                    nearUserVos.add(nearUserVo);
                    break;
                }
            }
        }
        return nearUserVos;
    }
    /**
     * 探花  左划右划小卡片
     *
     * @return
     */
    public List<TodayBest> queryCardsList(){
        User user = UserThreadLocal.get();
        //设置查询多少人，可根据用户的vip等级决定
        int count = 50;
        List<RecommendUser> recommendUserlist = this.recommendUserApi.queryCardList(user.getId(), count);
        //没有查询到设置几个默认的用户
        if (CollUtil.isEmpty(recommendUserlist)){
            recommendUserlist = new ArrayList<>();
            String[] recommentUserIdList = StrUtil.split(defaultRecommendUsers, ",");
            for (String s : recommentUserIdList) {
                RecommendUser recommendUser = new RecommendUser();
                recommendUser.setToUserId(user.getId());
                recommendUser.setUserId(Convert.toLong(s));
                recommendUserlist.add(recommendUser);
            }
        }
        //计算展现的数量，默认展现10个
        int showCount = Math.min(10, recommendUserlist.size());
        List<RecommendUser> result = new ArrayList<>();
        for (int i = 0; i < showCount; i++) {
            //TODO 可能重复
            int index = RandomUtil.randomInt(0, recommendUserlist.size());
            RecommendUser recommendUser = recommendUserlist.get(index);
            result.add(recommendUser);
        }
        List<Object> userIds = CollUtil.getFieldValues(result, "userId");
        List<TodayBest> todayBestList = new ArrayList<>();
        List<UserInfo> userInfoList = this.userInfoService.queryUserInfoByUserIdList(userIds);
        for (UserInfo userInfo : userInfoList) {
            TodayBest todayBest = new TodayBest();
            todayBest.setAvatar(userInfo.getLogo());
            todayBest.setId(userInfo.getUserId());
            todayBest.setAge(userInfo.getAge());
            todayBest.setGender(userInfo.getSex().name().toLowerCase());
            todayBest.setFateValue(0L);
            todayBest.setTags(Convert.toStrArray(StrUtil.split(userInfo.getTags(),",")));
            todayBest.setNickname(userInfo.getNickName());
            todayBestList.add(todayBest);
        }
        return todayBestList;
    }

    /**
     * 喜欢
     * 右滑
     * @param likeUserId
     * @return
     */
    public Boolean likeUser(Long likeUserId) {
        User user = UserThreadLocal.get();
        Boolean result = this.userLikeApi.likeUser(user.getId(), likeUserId);
        if (!result) {
            return false;
        }

        if (this.userLikeApi.isMutualLike(user.getId(), likeUserId)) {
            //相互喜欢成为好友
            this.imService.contactUser(likeUserId);
        }
        return true;
    }

    /**
     * 不喜欢
     * 左滑
     *
     * @param likeUserId
     * @return
     */
    public Boolean notLikeUser(Long likeUserId) {
        User user = UserThreadLocal.get();
        return this.userLikeApi.notLikeUser(user.getId(), likeUserId);
    }

}
