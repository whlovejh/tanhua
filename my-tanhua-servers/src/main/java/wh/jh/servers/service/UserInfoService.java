package wh.jh.servers.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wh.jh.commons.mapper.UserInfoMapper;
import wh.jh.commons.pojo.UserInfo;

import java.util.Collection;
import java.util.List;

@Service
public class UserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    public UserInfo queryUserInfoByUserId(Long userId) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        UserInfo userInfo = userInfoMapper.selectOne(wrapper);
        return userInfo;
    }

    public List<UserInfo> queryUserInfoList(QueryWrapper<UserInfo> queryWrapper) {
        return this.userInfoMapper.selectList(queryWrapper);
    }

    /**
     * 根据用户id列表查询用户信息
     *
     * @param userIdList
     * @return
     */
    public List<UserInfo> queryUserInfoByUserIdList(Collection<?> userIdList) {
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("user_id", userIdList);
        return this.queryUserInfoList(queryWrapper);
    }

    /**
     * @author  官文皓
     * @create  2021/5/30
     * @desc    更新用户信息
     **/
    public boolean updateUserInfoByUserId(UserInfo userInfo) {
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userInfo.getUserId());
        return this.userInfoMapper.update(userInfo, queryWrapper) > 0;
    }
}
