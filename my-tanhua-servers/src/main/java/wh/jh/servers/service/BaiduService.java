package wh.jh.servers.service;

import com.alibaba.dubbo.config.annotation.Reference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import wh.jh.commons.pojo.User;
import wh.jh.commons.utils.UserThreadLocal;
import wh.jh.dubbo.server.api.UserLocationApi;
import wh.jh.dubbo.server.pojo.UserLocation;

@Service
@Slf4j
public class BaiduService {
    @Reference(version = "1.0.0")
    private UserLocationApi userLocationApi;
    /**
     * 更新位置
     * @return
     */
    public Boolean updateLocation(Double longitude, Double latitude, String address) {
        User user = UserThreadLocal.get();
        try {
            Boolean updateResult = this.userLocationApi.updateUserLocation(user.getId(), longitude, latitude, address);
            return updateResult;
        } catch (Exception e) {
            log.error("更新地理位置失败~ userId = " + user.getId() + ", longitude = " + longitude + ", latitude = " + latitude + ", address = " + address, e);
        }
        return false;
    }
}
