package wh.jh.servers.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.One;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import wh.jh.commons.pojo.User;
import wh.jh.commons.pojo.UserInfo;
import wh.jh.commons.service.FileUploadService;
import wh.jh.commons.utils.UserThreadLocal;
import wh.jh.commons.vo.PicUploadResult;
import wh.jh.dubbo.server.api.VoiceApi;
import wh.jh.dubbo.server.pojo.Voice;
import wh.jh.servers.vo.VoiceVo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class VoiceService {
    @Reference(version = "1.0.0")
    private VoiceApi voiceApi;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private UserInfoService userInfoService;



    /**
     * @author  官文皓
     * @create  2021/6/1
     * @desc    上传语音 封装voice对象
     **/
    public Boolean saveVoice(MultipartFile file){
        User user = UserThreadLocal.get();

        //将语音上传到阿里云OSS中
        PicUploadResult uploadResult = this.fileUploadService.upload(file);
        if (StringUtils.isEmpty(uploadResult.getName())){
            //上传失败
            return false;
        }
        Voice voice = new Voice();
        voice.setId(new ObjectId());
        voice.setUserId(user.getId());
        voice.setSoundUrl(uploadResult.getName());
        //根据当前登录用户id查询该用户的信息
        UserInfo userInfo = userInfoService.queryUserInfoByUserId(user.getId());
        if (ObjectUtil.isEmpty(userInfo)){
            return false;
        }
        voice.setAge(userInfo.getAge());
        voice.setNickname(userInfo.getNickName());
        voice.setAvatar(userInfo.getLogo());
        voice.setDate(System.currentTimeMillis());
        voice.setGender(userInfo.getSex().name().toLowerCase());
        Boolean aBoolean = this.voiceApi.saveVoice(voice);
        if (aBoolean){
            return true;
        }
        return false;
    }

    /**
     * @author  官文皓
     * @create  2021/6/1
     * @desc    随机获取语音
     **/
    public VoiceVo getVoice(){
        User user = UserThreadLocal.get();
        VoiceVo voiceVo = new VoiceVo();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        String dataTime = sf.format(System.currentTimeMillis());
        String voiceRedisKey = "voice_" + user.getId() + "_" + dataTime;
        //在redis存还能收到桃花传音条数
        if (!(this.redisTemplate.hasKey(voiceRedisKey))){
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");//时:分:秒:毫秒
            String time = sdf.format(new Date());
            String[] timeArray = StringUtils.split(time, ":");
            Integer HH = Integer.valueOf(timeArray[0]);
            Integer mm = Integer.valueOf(timeArray[1]);
            Integer ss = Integer.valueOf(timeArray[2]);
            Integer seconds = (24 - HH - 1)*60*60 + (60 - mm - 1)*60 + (60 - ss);
            this.redisTemplate.opsForValue().set(voiceRedisKey,"10");
            this.redisTemplate.expire(voiceRedisKey,seconds, TimeUnit.SECONDS);
        }
        Integer remainingTimes = Integer.valueOf(this.redisTemplate.opsForValue().get(voiceRedisKey));
        if (remainingTimes <= 0){
            return voiceVo;
        }
        Voice voice = this.voiceApi.getVoice(user.getId());
        if (ObjectUtil.isEmpty(voice.getId())){
            voiceVo.setRemainingTimes(remainingTimes);
            return voiceVo;
        }
        voiceVo.setAge(voice.getAge());
        voiceVo.setAvatar(voice.getAvatar());
        voiceVo.setGender(voice.getGender());
        voiceVo.setId(voice.getUserId());
        voiceVo.setNickname(voice.getNickname());
        voiceVo.setSoundUrl(voice.getSoundUrl());
        voiceVo.setRemainingTimes(remainingTimes - 1);  //剩余次数
        this.redisTemplate.opsForValue().decrement(voiceRedisKey,1);

        return voiceVo;
    }
}
