package wh.jh.servers.controller;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import wh.jh.dubbo.server.pojo.Voice;
import wh.jh.servers.service.VoiceService;
import wh.jh.servers.vo.VoiceVo;

@RestController
public class VoiceController {
    @Autowired
    private VoiceService voiceService;

    /**
     * @author  官文皓
     * @create  2021/6/1
     * @desc    发语音
     **/
    @PostMapping("peachblossom")
    public ResponseEntity<Void> saveVoice(@RequestParam("soundFile")MultipartFile file){
        try {
            Boolean result = this.voiceService.saveVoice(file);
            if (result){
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * @author  官文皓
     * @create  2021/6/1
     * @desc    接收语音
     **/
    @GetMapping("peachblossom")
    public ResponseEntity<VoiceVo> getVoice(){
        try {
            VoiceVo voiceVo = this.voiceService.getVoice();
            if (ObjectUtil.isNotEmpty(voiceVo)){
                return ResponseEntity.ok(voiceVo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
