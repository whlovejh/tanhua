package wh.jh.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import wh.jh.commons.pojo.UserInfo;


@Component
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
