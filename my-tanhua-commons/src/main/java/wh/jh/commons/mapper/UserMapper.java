package wh.jh.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import wh.jh.commons.pojo.User;


@Mapper
@Component
public interface UserMapper extends BaseMapper<User> {
}
