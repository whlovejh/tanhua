package wh.jh.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import wh.jh.commons.pojo.Settings;
@Component
public interface SettingsMapper extends BaseMapper<Settings> {
}
