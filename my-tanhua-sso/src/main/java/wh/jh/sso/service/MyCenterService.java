package wh.jh.sso.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import wh.jh.commons.pojo.User;
import wh.jh.sso.vo.ErrorResult;

@Service
public class MyCenterService {

    @Autowired
    private UserService userService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    /**
     * 发送短信验证码
     *
     * @return
     */
    public Boolean sendVerificationCode(String token) {
        //校验token
        User user = this.userService.queryUserByToken(token);
        if(ObjectUtil.isEmpty(user)){
            return false;
        }

        ErrorResult errorResult = this.smsService.sendCheckCode(user.getMobile());
        return errorResult == null;
    }

    /**
     * 校验验证码
     *
     * @param code
     * @param token
     * @return
     */
    public Boolean checkVerificationCode(String code, String token) {
        //校验token
        User user = this.userService.queryUserByToken(token);
        if(ObjectUtil.isEmpty(user)){
            return false;
        }

        //校验验证码，先查询redis中的验证码
        String redisKey = "CHECK_CODE_" + user.getMobile();
        String value = this.redisTemplate.opsForValue().get(redisKey);

        if(StrUtil.equals(code, value)){
            //将验证码删除
            this.redisTemplate.delete(redisKey);
            return true;
        }

        return false;
    }

    public Boolean updatePhone(String token, String newPhone) {
        //校验token
        User user = this.userService.queryUserByToken(token);
        if(ObjectUtil.isEmpty(user)){
            return false;
        }
        Boolean result = this.userService.updatePhone(user.getId(), newPhone);
        if(result){
            String redisKey = "TANHUA_USER_MOBILE_" + user.getId();
            this.redisTemplate.delete(redisKey);
        }

        return result;
    }
}
